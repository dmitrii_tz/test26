<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\API\BaseController as BaseController;
use App\Mark;

class MarkController extends BaseController
{

    public function index()
    {
        $products = Mark::all();
        return $this->sendResponse($products->toArray(), 'Держи марки');
    }
}