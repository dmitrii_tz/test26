<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


//Route::resource('api/marks', 'API\MarkController@index');


Route::resource('marks', 'API\MarkController');
Route::resource('models/', 'API\ModelsController');
Route::resource('cars', 'API\CarsController');

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
