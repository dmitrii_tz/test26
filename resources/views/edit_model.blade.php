<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    @include('layouts.app')
</head>
<body>
<div class="container">
    <div class="row">

        @include('layouts.menu')

        <div class="col-md-12 center">
            <h3>{{$title}}</h3>
            <table class="table">
                <tr>
                    <th>Марка</th>
                    <th>Имя</th>
                    <th>Действие</th>
                </tr>

                <tr>
                    <form action="/models/save/{{$model->id}}" method="post">
                        {{csrf_field()}}
                        <td>
                            <select class="form-control" name="mark_id">
                                <option value="{{$model->mark->id}}">{{$model->mark->name_mark}}</option>

                                @foreach($marks as $mark)
                                    <option value="{{$mark->id}}">{{$mark->name_mark}}</option>
                                @endforeach
                            </select>
                        </td>
                        <td>
                            <input class="form-control" name="name_model" type="text" value="{{$model->name_model}}" required>
                        </td>
                        <td>
                            <button class="btn btn-success" type="submit">Сохранить</button>
                        </td>
                    </form>
                </tr>

            </table>

        </div>

    </div>
</div>
</body>
</html>
