<?php

namespace App\Http\Controllers;

use App\ModelCar;
use App\Mark;
use Illuminate\Http\Request;

class ModelController extends Controller
{
    public function index(){
        $models = ModelCar::all();
        $marks = Mark::all();

        return view('model',['title' => 'Справочник моделей', 'models' => $models, 'marks' => $marks]);
    }

    public function delete($id){

        ModelCar::destroy($id);

        return redirect()->back();
    }

    public function create(Request $request){

        ModelCar::create($request->input());

        return redirect()->back();
    }

    public function edit($id){

        $model = ModelCar::find($id);
        $marks = Mark::where('id', '!=', $model->mark->id)->get();
        return view('edit_model',['title' => 'Редактирование моделей', 'model' => $model, 'marks' => $marks]);
    }

    public function save(Request $request, $id){

        $model = ModelCar::find($id);

        $model->update($request->input());

        return redirect('/models');
    }
}
