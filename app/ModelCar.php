<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ModelCar extends Model
{
    use SoftDeletes;

    protected $table = 'models';
    protected $guarded = ['_token'];

    public function mark(){
        return $this->belongsTo('App\Mark');
    }
}
