<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Car;
use App\Mark;
use App\ModelCar;

class CarController extends Controller
{

    public function index(){
        $cars = Car::all();
        $marks = Mark::all();
        $models = ModelCar::all();

        return view('car',['title' => 'Справочник автомобилей', 'cars' => $cars, 'marks' => $marks, 'models' => $models]);
    }

    public function delete($id){

        Car::destroy($id);

        return redirect()->back();
    }

    public function create(Request $request){

        Car::create($request->input());

        return redirect()->back();
    }

    //не стал доделывать обновление, такая функция была в моделях уже сделана
    public function edit($id){

        $car = Car::find($id);
        $marks = Mark::all();
        $models = ModelCar::all();

        return view('edit_car',['title' => 'Редактирование авто', 'mark' => $car]);
    }

    public function save(Request $request, $id){

        $car = Car::find($id);

        $car->update($request->input());

        return redirect('/');
    }

    public function ajax($id){

        $models = ModelCar::where('mark_id', $id)->get()->toArray();
        return $models;
    }
}
