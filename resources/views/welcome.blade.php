<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    @include('layouts.app')
</head>
<body>
<div class="container">

    @include('layouts.menu')

    <div class="col-md-12 center">
        <h3>{{$title}}</h3>
        <table id="table" class="table display">
            <tr>
                <th>ID</th>
                <th>Имя</th>
                <th>Действие</th>
            </tr>
            <tr>
                <form action="/marks/create/" method="post">
                    {{csrf_field()}}
                    <td>
                    </td>
                    <td>
                        <input class="form-control" name="name_mark" type="text" required>
                    </td>
                    <td>
                        <button class="btn btn-success" type="submit" style="width: 220px">Создать</button>
                    </td>
                </form>
            </tr>
            @foreach($contents as $content)
                <tr>
                    <td>{{$content->id}}</td>
                    <td>{{$content->name_mark}}</td>
                    <td>
                        <form action="/marks/delete/{{$content->id}}/">
                            <button class="btn btn-warning">Удалить</button>
                        </form>

                        <form action="/marks/edit/{{$content->id}}/" method="get">
                            <button class="btn btn-info">Редактировать</button>
                        </form>
                    </td>
                </tr>
            @endforeach
        </table>

    </div>
</div>
</body>
</html>
