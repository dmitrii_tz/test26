<script>
    $(function () {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $('#mark').change(function () {
            var id_model = $(this).val();
            $.ajax({
                url: "/cars/ajax/" + id_model,
                type: "get",
                success: function (data) {
                    //alert(data['0']['id']);
                    $('#deviceSelect').empty();
                    $.each(data, function (i) {
                        $('#deviceSelect').append('<option value="' + data[i]['id'] + '">' + data[i]['name_model'] + '</option>');
                    });
                }
            });
        });
    });
</script>
