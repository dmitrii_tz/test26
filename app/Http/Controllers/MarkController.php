<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Redirect;
use App\Mark;

class MarkController extends Controller
{
    public function index(){

        $marks = Mark::all();

        return view('welcome',['title' => 'Справочник марок', 'contents' => $marks]);
    }
    public function delete($id){

        Mark::destroy($id);

        return redirect()->back();
    }

    public function create(Request $request){


        Mark::create($request->input());

        return redirect()->back();
    }

    public function edit($id){

        $mark = Mark::find($id);

        return view('edit_mark',['title' => 'Редактирование марки', 'mark' => $mark]);
    }

    public function save(Request $request, $id){

        $mark = Mark::find($id);

        $mark->update($request->input());

        return redirect('/');
    }
}
