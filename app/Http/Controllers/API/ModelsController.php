<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\API\BaseController as BaseController;
use App\ModelCar;

class ModelsController extends BaseController
{

    public function index(Request $request)
    {
        if(isset($request->mark)){
            $products = ModelCar::where('mark_id', $request->mark)->get();

            if($products->isEmpty()) {
                return $this->sendError('Empty Array', 'Записи с таким ID нет', 404);
            }
        }
        else{
            $products = ModelCar::all();
        }

        return $this->sendResponse($products->toArray(), 'Держи машины');
    }
}