<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Car extends Model
{
    use SoftDeletes;

    protected $guarded = ['_token'];

    public function mark(){
        return $this->belongsTo('App\Mark');
    }

    public function model(){
        return $this->belongsTo('App\ModelCar');
    }
}
