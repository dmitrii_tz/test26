<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    @include('layouts.app')
</head>
<body>

@include('layouts.menu')

<div class="center">
    <div class="center">
        <h3>{{$title}}</h3>

        <table class="table" style=" margin-bottom: 0px;">

            <form action="/cars/create/" method="post">
                <tr>
                    {{csrf_field()}}
                    <td>
                    </td>
                    <td>
                        <select class="form-control" name="mark_id" id="mark">
                            <option value="">Выберите марку...</option>
                            @foreach($marks as $mark)
                                <option value="{{$mark->id}}">{{$mark->name_mark}}</option>
                            @endforeach
                        </select>
                    </td>
                    <td>
                        <select id="deviceSelect" name="model_id" class="form-control" required>
                            <option value="">Выберите модель...</option>
                        </select>
                    </td>
                    <td>
                        <input class="form-control" name="name" type="text" required>
                    </td>
                    <td>
                        <input class="form-control" name="year" type="number" required>
                    </td>
                    <td>
                        <input class="form-control" name="mileage" type="number" required>
                    </td>
                    <td>
                        <input class="form-control" name="color" type="text" required>
                    </td>
                    <td>
                        <input class="form-control" name="cost" type="number" required>
                    </td>
                    <td>
                        <button class="btn btn-success" type="submit" style="width: 220px">Создать</button>
                    </td>

                </tr>
            </form>
        </table>

        <table id="table" class="table display">
            <tr>
                <th>ID
                    <img src="{{URL::asset('128411.png')}}" alt="profile Pic" height="14" width="14">
                </th>
                <th>Марка
                    <img src="{{URL::asset('128411.png')}}" alt="profile Pic" height="14" width="14">
                </th>
                <th>Модель
                    <img src="{{URL::asset('128411.png')}}" alt="profile Pic" height="14" width="14">
                </th>
                <th>Название
                    <img src="{{URL::asset('128411.png')}}" alt="profile Pic" height="14" width="14">
                </th>
                <th>Год выпуска
                    <img src="{{URL::asset('128411.png')}}" alt="profile Pic" height="14" width="14">
                </th>
                <th>Пробег
                    <img src="{{URL::asset('128411.png')}}" alt="profile Pic" height="14" width="14">
                </th>
                <th>Цвет
                    <img src="{{URL::asset('128411.png')}}" alt="profile Pic" height="14" width="14">
                </th>
                <th>Цена
                    <img src="{{URL::asset('128411.png')}}" alt="profile Pic" height="14" width="14">
                </th>
                <th>Действие</th>
            </tr>

            @foreach($cars as $car)
                <tr>
                    <td>{{$car->id}}</td>
                    <td>{{$car->mark->name_mark}}</td>
                    <td>{{$car->model->name_model}}</td>
                    <td>{{$car->name}}</td>
                    <td>{{$car->year}}</td>
                    <td>{{$car->mileage}}</td>
                    <td>{{$car->color}}</td>
                    <td>{{$car->cost}}</td>
                    <td>
                        <form action="/cars/delete/{{$car->id}}/">
                            <button class="btn btn-warning">Удалить</button>
                        </form>

                        <form action="/cars/edit/{{$car->id}}/" method="get">
                            <button class="btn btn-info">Редактировать</button>
                        </form>
                    </td>
                </tr>
            @endforeach
        </table>
    </div>
</div>
 @include('layouts.select')
<script>
    $('th').click(function(){
        var table = $(this).parents('table').eq(0)
        var rows = table.find('tr:gt(0)').toArray().sort(comparer($(this).index()))
        this.asc = !this.asc
        if (!this.asc){rows = rows.reverse()}
        for (var i = 0; i < rows.length; i++){table.append(rows[i])}
    })
    function comparer(index) {
        return function(a, b) {
            var valA = getCellValue(a, index), valB = getCellValue(b, index)
            return $.isNumeric(valA) && $.isNumeric(valB) ? valA - valB : valA.toString().localeCompare(valB)
        }
    }
    function getCellValue(row, index){ return $(row).children('td').eq(index).text() }
</script>
</body>
</html>
