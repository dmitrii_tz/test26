<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\API\BaseController as BaseController;
use App\Car;

class CarsController extends BaseController
{

    //Не знаю как сделать этот метод с большим колличеством опциональных параметров
    //Вот что я пытался написать

    public function index(Request $request)
    {
        if($request->input() == null){
            $products = Car::all();
        }
        else{
            $id = [
                'mark_id' => $request->mark_id,
                'model_id' => $request->model_id
            ];

            $preset_id = [
                'mark_id' => null,
                'model_id' => null
            ];

            $results_id = array_merge($preset_id, $id);
            foreach ($results_id as $i => $result){
                if($result == null){
                    unset($results_id[$i]);
                }
            }

            $products = Car::where($results_id)->get();

            if($request->year_from != null && $request->year_to != null){
                $year = [
                    'year_from' => $request->year_from,
                    'year_to' => $request->year_to
                ];

                $preset_year = [
                    'mark_id' => null,
                    'model_id' => null
                ];

                $results_year = array_merge($preset_year, $year);
                foreach ($results_year as $i => $result){
                    if($result == null){
                        unset($results_year[$i]);
                    }
                }
                $products->whereBetween('year',$results_year)
                    ->get();
            }

            if($products->isEmpty()) {
                return $this->sendError('Empty Array', 'Записи с таким ID нет', 404);
                $products = Car::all();
            }
        }

        return $this->sendResponse($products->toArray(), 'Держи машину');
    }
}