<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    @include('layouts.app')
</head>
<body>
<div class="container">
    <div class="row">

        @include('layouts.menu')

        <div class="col-md-12 center">
            <h3>{{$title}}</h3>
            <table id="table" class="table display">
                <tr>
                    <th>Имя</th>
                    <th>Действие</th>
                </tr>

                <tr>
                    <form action="/marks/save/{{$mark->id}}" method="post">
                        {{csrf_field()}}
                        <td>
                            <input class="form-control" name="name_mark" type="text" value="{{$mark->name_mark}}" required>
                        </td>
                        <td>
                            <button class="btn btn-success" type="submit">Сохранить</button>
                        </td>
                    </form>
                </tr>

            </table>

        </div>

    </div>
</div>
</body>
</html>
