<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>

    @include('layouts.app')

</head>
<body>
<div class="container">
    <div class="row">
@include('layouts.menu')

        <div class="col-md-12 center">
        <h3>{{$title}}</h3>

        <table class="table">
            <tr>
                <th>ID</th>
                <th>Марка</th>
                <th>Название</th>
                <th>Действие</th>
            </tr>
            <tr>
                <form action="/models/create/" method="post">
                    {{csrf_field()}}
                    <td>
                    </td>
                    <td>
                        <select class="form-control" name="mark_id">
                            <option id="opt" value="">Выберите марку...</option>
                            @foreach($marks as $mark)
                                <option id="opt" value="{{$mark->id}}">{{$mark->name_mark}}</option>
                            @endforeach
                        </select>
                    </td>
                    <td>
                        <input class="form-control" name="name_model" type="text" required>
                    </td>
                    <td>
                        <button class="btn btn-success" type="submit" style="width: 220px">Создать</button>
                    </td>
                </form>
            </tr>
            @foreach($models as $model)
                <tr>
                    <td>{{$model->id}}</td>
                    <td>{{$model->mark->name_mark}}</td>
                    <td>{{$model->name_model}}</td>
                    <td>
                        <form action="/models/delete/{{$model->id}}/">
                            <button class="btn btn-warning">Удалить</button>
                        </form>

                        <form action="/models/edit/{{$model->id}}/" method="get">
                            <button class="btn btn-info">Редактировать</button>
                        </form>
                    </td>
                </tr>
            @endforeach
        </table>
    </div>
</body>
</html>
