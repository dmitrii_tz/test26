<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'MarkController@index');
Route::get('/marks/delete/{id}', 'MarkController@delete');
Route::post('/marks/create/', 'MarkController@create');
Route::get('/marks/edit/{id}', 'MarkController@edit');
Route::post('/marks/save/{id}', 'MarkController@save');

Route::get('/models', 'ModelController@index');
Route::get('/models/delete/{id}', 'ModelController@delete');
Route::post('/models/create/', 'ModelController@create');
Route::get('/models/edit/{id}', 'ModelController@edit');
Route::post('/models/save/{id}', 'ModelController@save');

Route::get('/cars', 'CarController@index');
Route::get('/cars/delete/{id}', 'CarController@delete');
Route::post('/cars/create/', 'CarController@create');
Route::get('/cars/ajax/{id}', 'CarController@ajax');